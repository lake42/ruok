package healthcheck

import (
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/rs/zerolog"
)

// Send a HTTP Get request to a local health endpoint.
// Return 0 if a 2xx status code is received, 1 otherwise.
func LocalHealthcheck(scheme string, port int64, path string, log *zerolog.Logger) int {
	// Call healthcheck endpoint
	url := fmt.Sprintf("%s://localhost:%s%s", scheme, strconv.FormatInt(port, 10), path)
	resp, err := http.Get(url)
	if err != nil {
		log.Error().AnErr("cause", err).Msg("http healthcheck request failed")
		return 1
	}
	// Read body
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().AnErr("cause", err).Msg("failed to read response body")
		return 1
	}
	// Check response
	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		// Healthcheck passed
		log.Debug().Str("body", string(body)).Msg("healthckeck successful")
		return 0
	} else {
		// Healthcheck failed
		log.Error().Int("code", resp.StatusCode).Str("body", string(body)).Msg("http healthcheck failed")
		return 1
	}
}
