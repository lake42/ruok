package constants

// Env. variables names
const (
	RUOK_SCHEME_ENVVAR  = "RUOK_SCHEME"
	RUOK_PORT_ENVVAR    = "RUOK_PORT"
	RUOK_PATH_ENVVAR    = "RUOK_PATH"
	RUOK_VERBOSE_ENVVAR = "RUOK_VERBOSE"
)

// Flag names
const (
	RUOK_SCHEME_FLAG  = "scheme"
	RUOK_PORT_FLAG    = "port"
	RUOK_PATH_FLAG    = "path"
	RUOK_VERBOSE_FLAG = "verbose"
)

// Env. variables defaults
const (
	RUOK_SCHEME_DEFAULT  = "http"
	RUOK_PORT_DEFAULT    = 8080
	RUOK_PATH_DEFAULT    = "/health"
	RUOK_VERBOSE_DEFAULT = false
)
