package main

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	"gitlab.com/lake42/ruok/internal/constants"
	"gitlab.com/lake42/ruok/internal/flags"
	"gitlab.com/lake42/ruok/internal/healthcheck"
)

func main() {

	app := &cli.App{
		Name:  "ruok",
		Usage: "ping your local healthcheck http endpoint and return 0 in case of scuess or -1 in case of failure.",
		Action: func(ctx *cli.Context) error {
			// Extract flags values
			verbose := ctx.Bool(constants.RUOK_VERBOSE_FLAG)
			path := ctx.String(constants.RUOK_PATH_FLAG)
			port := ctx.Int64(constants.RUOK_PORT_FLAG)
			scheme := ctx.String(constants.RUOK_SCHEME_FLAG)
			// Configure logger to DEBUG or ERROR level depending on verbose flag
			if verbose {
				zerolog.SetGlobalLevel(zerolog.DebugLevel)
			} else {
				zerolog.SetGlobalLevel(zerolog.ErrorLevel)
			}
			// DEBUG - Log flags
			log.Debug().
				Str(constants.RUOK_PATH_FLAG, path).
				Bool(constants.RUOK_VERBOSE_FLAG, verbose).
				Int64(constants.RUOK_PORT_FLAG, port).
				Str(constants.RUOK_SCHEME_FLAG, scheme).
				Msg("loaded configuration")
			// Call healthcheck endpoint
			os.Exit(healthcheck.LocalHealthcheck(scheme, port, path, &log.Logger))
			return nil
		},
		Flags: []cli.Flag{
			flags.GetPortFlag(),
			flags.GetPathFlag(),
			flags.GetSchemeFlag(),
			flags.GetVerboseFlag(),
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Error().AnErr("cause", err).Msg("ruok unexpected error")
		os.Exit(-1)
	}
}
